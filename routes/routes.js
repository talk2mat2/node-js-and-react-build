const express = require("express")
const router = express.Router()
const {GoogleAutoComplete,Getweatherinfo,Historicweatherinfo} = require("../controllers/weathercontroller")

router.get("/GoogleAutoComplete",GoogleAutoComplete)
router.get("/Getweatherinfo",Getweatherinfo)
router.get("/Historicweatherinfo",Historicweatherinfo)



module.exports =router