const axios = require("axios");

//fecth places by search keywords as user types
exports.GoogleAutoComplete = async (req, res) => {
  const { search } = req.query;

  if (!search) {
    return res.status(501).json({
      status: false,
      message: "no search paramater provided",
    });
  }

  var config = {
    method: "get",
    url: `https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${search}&types=geocode&key=${process.env.GOOGLE_KEY}`,
    headers: {},
  };

  axios(config)
    .then(function (response) {
      return res.status(200).json({
        status: true,
        message: "success",
        data: response.data,
      });
    })
    .catch(function (error) {
      console.log(error);
      return res.status(501).json({
        status: false,
        message: "no search paramater provided",
      });
    });
};

//geolocate by places id
exports.Getweatherinfo = async (req, res) => {
  const { places_id } = req.query;

  if (!places_id) {
    return res.status(501).json({
      status: false,
      message: "places_id not provided",
    });
  }

  var config = {
    method: "get",
    url: `https://maps.googleapis.com/maps/api/geocode/json?place_id=${places_id}&key=${process.env.GOOGLE_KEY}`,
    headers: {},
  };

  axios(config)
    .then(function (response) {
      const lat = response.data["results"][0]["geometry"]["location"]["lat"];
      const lng = response.data["results"][0]["geometry"]["location"]["lng"];

      const config2 = {
        method: "get",

        url: `https://api.openweathermap.org/data/2.5/onecall?lat=${lat}&lon=${lng}&exclude={part}&appid=${process.env.OPEN_KEY}`,
        headers: {},
      };
      axios(config2).then(function (response) {
        return res.status(200).json({
          status: true,
          message: "success",
          data: response.data,
        });
      });
    })
    .catch(function (error) {
      console.log(error);
      return res.status(501).json({
        status: false,
        message: "no search paramater provided",
      });
    });
};

//get historic weather info
exports.Historicweatherinfo = async (req, res) => {
  const { places_id } = req.query;
  //we initialize an array because we are goint to make five api calls /one for each past days up to five days back
  //to get 5 days historic data
  //so we will push response.current objects to the array till its up five days
  const historyData = [];
 

  if (!places_id) {
    return res.status(501).json({
      status: false,
      message: "places_id not provided",
    });
  }

  var config = {
    method: "get",
    url: `https://maps.googleapis.com/maps/api/geocode/json?place_id=${places_id}&key=${process.env.GOOGLE_KEY}`,
    headers: {},
  };

  axios(config)
    .then(function (response) {
      const lat = response.data["results"][0]["geometry"]["location"]["lat"];
      const lng = response.data["results"][0]["geometry"]["location"]["lng"];
      const date= new Date()
      const todayUnix = Math.floor(date / 1000);
      const config2 = {
        method: "get",

        url: `https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=${lat}&lon=${lng}&dt=${todayUnix}&appid=${process.env.OPEN_KEY}`,
        headers: {},
      };
      axios(config2)
        .then(function (response) {
          historyData.push(response.data.current);
        })

        // here
          .then(async () => {
            const date= new Date()
          const secondnda = date.setDate(date.getDate() - 1);

          const secondndayUnix = Math.floor(secondnda / 1000);
          const config3 = {
            method: "get",

            url: `https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=${lat}&lon=${lng}&dt=${secondndayUnix}&appid=${process.env.OPEN_KEY}`,
            headers: {},
          };

          await axios(config3)
            .then(function (response) {
              historyData.push(response.data.current);
            })
              .then(async () => {
                const date= new Date()
              const thirddayday = date.setDate(date.getDate() - 2);
              const thirdndayUnix = Math.floor(thirddayday / 1000);
              const config4 = {
                method: "get",

                url: `https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=${lat}&lon=${lng}&dt=${thirdndayUnix}&appid=${process.env.OPEN_KEY}`,
                headers: {},
              };

              await axios(config4).then(function (response) {
                historyData.push(response.data.current);
              });
            })
            .then(async () => {
                const date= new Date()
                const fouthDay = date.setDate(date.getDate() - 3);
                const fouthDayUnix = Math.floor(fouthDay / 1000);
                const config6 = {
                  method: "get",
      
                  url: `https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=${lat}&lon=${lng}&dt=${fouthDayUnix}&appid=${process.env.OPEN_KEY}`,
                  headers: {},
                };
      
                await axios(config6).then(function (response) {
                  historyData.push(response.data.current);
                });
              })
          
          }).then(async () => {
            const date= new Date()
            const fiftday = date.setDate(date.getDate() - 4);
            const fiftdayyUnix = Math.floor(fiftday / 1000);
            const config6 = {
              method: "get",
  
              url: `https://api.openweathermap.org/data/2.5/onecall/timemachine?lat=${lat}&lon=${lng}&dt=${fiftdayyUnix}&appid=${process.env.OPEN_KEY}`,
              headers: {},
            };
  
           await axios(config6).then(function (response) {
              historyData.push(response.data.current);
            });
          })

        .then(() => {
          return res.status(200).json({
            status: true,
            message: "success",
            data: historyData,
          });
        });
      // ended
    })
    .catch(function (error) {
      console.log(error);
      return res.status(501).json({
        status: false,
        message: "error ocurred" + error,
      });
    });
};
