const express = require("express")
const app = express()
const router = require("./routes/routes")
const cors = require("cors")
const path = require("path")

process.env.NODE_ENV!=="Production"?require("dotenv").config():null

const port = process.env.PORT || 8080;
app.use(cors())
app.use(express.static(path.join(__dirname,"./front-end")))
app.use(express.urlencoded({extended:false}))
app.use(express.json({}))
app.use("/api", router)
app.get("/", (req, res) => {
    res.sendFile(path.join(__dirname,"./front-end/","index.html"))
})


app.listen(port, (err, success) => {
    if (err) {
       throw err
    }
    console.log(`serving port ${port}`)
})